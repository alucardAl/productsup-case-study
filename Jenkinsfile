pipeline {
    agent any
    
    environment {
        DOCKER_IMAGE_NAME = 'productsup-case-study'
        DOCKERFILE_PATH = 'Dockerfile'
        DOCKER_COMPOSE_FILE = 'docker-compose.yaml'
        MARIADB_COMPOSE_FILE = 'mariadb-compose.yaml'
        WEB_SERVER_URL = 'http://34.205.242.114:82'
    }

    stages {
        stage('Checkout') {
            steps {
                git branch: 'main', url: 'git@gitlab.com:alucardAl/productsup-case-study.git'
            }
        }

        stage('Build Docker Image') {
            steps {
                script {
                    docker.build("${env.DOCKER_IMAGE_NAME}", "--no-cache -f ${env.DOCKERFILE_PATH} .")
                }
            }
        }
        
        stage('Deploy Docker') {
            steps {
                script {
                    sh "docker compose -f $env.MARIADB_COMPOSE_FILE down && docker compose -f $env.MARIADB_COMPOSE_FILE up -d"
                    sh "docker compose -f $env.DOCKER_COMPOSE_FILE down && docker compose -f $env.DOCKER_COMPOSE_FILE up -d"
                }
            }
        }

        stage('Test JSON Response') {
            steps {
                script {
                    def response = sh(script: "curl -s ${env.WEB_SERVER_URL}", returnStdout: true).trim()
                    def jsonResponse = readJSON text: response
                    if (!(jsonResponse instanceof List)) {
                        error("The response is not a valid JSON array: ${response}")
                    }
                    echo "Valid JSON array response: ${response}"
                }
            }
        }

        stage('Cleanup Workspace') {
            steps {
                cleanWs()
            }
        }
    }

    post {
        success {
            echo 'Docker image build and deploy successful!'
        }
        failure {
            echo 'Docker image build and deploy failed!'
        }
    }
}

