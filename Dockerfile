FROM alpine:3.20.0
RUN apk upgrade && apk add php83 php83-fpm php83-json php83-curl php83-openssl php83-mbstring php83-iconv php83-phar php83-pdo_mysql nginx
RUN sed -i -e 's/memory_limit = .*/memory_limit = 1024M/' /etc/php83/php.ini && \
    sed -i -e 's/max_execution_time = .*/max_execution_time = 45/' /etc/php83/php.ini && \
    sed -i -e 's/listen = .*/listen = 9000/' /etc/php83/php-fpm.d/www.conf
COPY productsup-nginx.conf /etc/nginx/http.d/default.conf
COPY project /var/www/devops-case-study
RUN adduser -G www-data -s /usr/sbin/nologin -D www-data
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
RUN ln -sf /dev/stderr /var/log/php83/error.log
WORKDIR /var/www/devops-case-study
RUN php83 -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php83 composer-setup.php && \
    php83 -r "unlink('composer-setup.php');" && \
    mv composer.phar /usr/sbin/composer
RUN php83 /usr/sbin/composer update
COPY entrypoint.sh /entrypoint.sh
CMD ["/entrypoint.sh"]
