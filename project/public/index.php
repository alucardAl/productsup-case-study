<?php
require '../vendor/autoload.php';
 
use Medoo\Medoo;
 
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'foobar',
    'server' => 'mariadb',
    'username' => 'foo_user',
    'password' => 'foo_pass'
]);
 
$database->insert('authors', [
    'first_name' => 'baz',
    'last_name' => 'lala',
    'email' => 'foo+bar@foo.com',
    'birthdate' => '1984-05-11'
]);

$database->insert('posts', [
    'author_id' => '1',
    'title' => 'this is some post',
    'description' => 'bla bla bla, whatever',
    'content' => 'well this does not really matter',
    'date' => '2018-04-10'
]);
 
$data = $database->select('posts', [
    'title',
    'date'
], [
    'author_id' => 1
]);
 
echo json_encode($data);
